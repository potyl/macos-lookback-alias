SHELL := bash

SOURCE_FOLDER  := .
PROJECT_NAME   := $(notdir $(CURDIR))
ROOT_DIR       := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

PLIST_LABEL     = $(shell /usr/libexec/PlistBuddy -c 'print :Label' loopback-alias.plist)
INSTALL_PLIST   = /Library/LaunchDaemons/$(PLIST_LABEL).plist
INSTALL_SCRIPT  = /usr/local/bin/loopback-alias.sh

COLOR_START     = \e[91m\e[1m
COLOR_END       = \e[0m
SAY             = @printf "$(COLOR_START)%s\n$(COLOR_END)"


##
## Ad hoc commands
##

.PHONY: info
info:
	$(SAY) "Info"
	@echo "PROJECT_NAME    $(PROJECT_NAME)"
	@echo "PLIST_LABEL     $(PLIST_LABEL)"
	@echo "INSTALL_PLIST   $(INSTALL_PLIST)"
	@echo "INSTALL_SCRIPT  $(INSTALL_SCRIPT)"


.PHONY: ifconfig
ifconfig:
	ifconfig lo0


.PHONY: hosts
hosts:
	./loopback-alias.sh --list /etc/hosts


##
## Installer
##

.PHONY: install
install:
	$(SAY) "Installing $(INSTALL_PLIST)"
	sudo install -m 0644 -o root -g wheel loopback-alias.plist "$(INSTALL_PLIST)"

	$(SAY) "Installing $(INSTALL_SCRIPT)"
	sudo install -m 0755 -o root -g wheel loopback-alias.sh "$(INSTALL_SCRIPT)"

	$(SAY) "Loading $(INSTALL_PLIST)"
	sudo launchctl load "$(INSTALL_PLIST)"


.PHONY: uninstall
uninstall:
	$(SAY) "Unloading $(INSTALL_PLIST)"
	-if [[ -e "$(INSTALL_PLIST)" ]]; then sudo launchctl unload "$(INSTALL_PLIST)"; fi

	$(SAY) "Uninstalling $(INSTALL_SCRIPT)"
	-sudo rm -f "$(INSTALL_SCRIPT)"

	$(SAY) "Uninstalling $(INSTALL_PLIST)"
	-sudo rm -f "$(INSTALL_PLIST)"
