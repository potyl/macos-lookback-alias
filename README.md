# Loopback Alias

This utility configures MacOS so that its network loopback interface (`lo0`) can accept other IP addresses in the range `127.0.0.0/8`, as documented in the [RFC3330](https://datatracker.ietf.org/doc/html/rfc3330).

This is performed by creating a loopback alias for each IP that's needed.
You can read more about the solution in this [StackOverflow answer](https://superuser.com/a/635327).

## Usage

You can create loopback aliases by running the script `loopback-alias.sh`:

```bash
./loopback-alias.sh 127.0.0.2 127.0.0.3
```

That's it, you now have two more loopback addresses to use!

The script accepts following input arguments:

- numeric: (ex: `2`) considered an offset to `127.0.0.0` (`2` will be converted to `127.0.0.2`)
- range: (ex: `3-10`) also considered an offset to `127.0.0.0` (`3-10` will be converted to `127.0.0.3` ... `127.0.0.10`)
- ip: (ex: `127.0.0.2`) a full IP address

### Using numeric values

It is possible to use the numeric values instead of IPs.
These values are considered to be offsets to `127.0.0.0` and are therefore added.

Thus the following commands are equivalent:

```bash
./loopback-alias.sh 127.0.0.2 127.0.0.3 127.0.0.4 127.0.0.5
./loopback-alias.sh 2 3 4 5
./loopback-alias.sh 2-5
```

### Removing loopback aliases

It is possible to remove network aliases with `--delete`.
Simply provide the IP addresses to remove.

```bash
./loopback-alias.sh --delete 127.0.0.2 127.0.0.3
```

It is also possible to delete the IPs from a file:

```bash
./loopback-alias.sh --delete --file /etc/hosts
```

### Using a file

It is possible to use a text file for listing the IP addresses with the argument `--file FILE`.
The file format expects one IP per line, blank lines are ignored and comments can be added with `#`.
When parsing a file the entry `127.0.0.1` will be silently discarded and any text after the IP address is ignored,
making the file format compatible (and ideal) for `/etc/hosts`.

Sample `/etc/hosts`:

```text
127.0.0.1       localhost
255.255.255.255 broadcasthost
::1             localhost 

# Local development
127.0.0.2 foo
127.0.0.3 bar
127.0.0.2 foo2

# Added by Docker Desktop
127.0.0.1 kubernetes.docker.internal
```

Multiple files can be combined as well as files and numerical IPs, IP ranges and raw IPs.

## Persistent loopback aliases

MacOS will not remember the aliases after a reboot and they will need to be recreated either by hand or to automate the process.

The easiest way to automate the loopback aliases is to use a launch daemon that will create the interfaces at startup.
The project provides everything required for setting up a launch daemon that will read the interfaces from a file.
By default the interfaces are taken from `/etc/hosts`.

You can change the file location by editing the `plist` file's command arguments by hand or through this command (replace `/etc/loopback-alias.conf`):

```bash
/usr/libexec/PlistBuddy loopback-alias.plist -c 'Set :ProgramArguments:2 /etc/loopback-alias.conf'
```

### Installing the launch daemon

You can install the launch daemon with:

```bash
make install
```

If you want to you can also uninstall it with:

```bash
make uninstall
```

## Loopback benefits

By using different loopback addresses and pairing them with entries in `/etc/hosts` or DNS aliases it is possible
run multiple services (or docker containers) sharing a same port (ex: 80, 443, 3306).

You can give it a try.

Create your hostname aliases by editing `/etc/hosts`:

```bash
$ grep example.org /etc/hosts
127.0.0.100 foo.example.org
127.0.0.101 bar.example.org
```

**NOTE**: this step is optional but it helps to give the IPs nicer names.

Create the loopback aliases:

```bash
./loopback-alias.sh 127.0.0.100 127.0.0.101
```

Run two web servers:

```bash
node -e 'require("http").createServer((_, res) => res.end("foo\n")).listen(8080, "foo.example.org")'

node -e 'require("http").createServer((_, res) => res.end("bar\n")).listen(8080, "bar.example.org")'
```

Query the two web servers:

```bash
$ curl http://foo.example.org:8080/
foo

$ curl http://bar.example.org:8080/
bar
```

### Privilege ports

This also works for [privileged ports](https://www.w3.org/Daemon/User/Installation/PrivilegedPorts.html) (< 1024)
also the programs that will listen on these ports need to be started as `root` or with `sudo`.

Run two web servers:

```bash
sudo node -e 'require("http").createServer((_, res) => res.end("foo\n")).listen(80, "foo.example.org")'

sudo node -e 'require("http").createServer((_, res) => res.end("bar\n")).listen(80, "bar.example.org")'
```

Query the two web servers:

```bash
$ curl http://foo.example.org/
Hi from foo

$ curl http://bar.example.org/
Hi from bar
```

### Docker

We can also use docker and to bind the port to the IP address.
The only caveat is that the IP address needs to be prefixed to the `-p ip:hostPort:containerPort`:

```bash
-p 127.0.0.100:80:80
```

```bash
docker stop -t 0 foo bar

ip_foo=$(getent hosts foo.example.org | sed -E -e 's/[[:blank:]]+.*$//')
docker run --name foo --detach --rm -p "$ip_foo:80:80"  node -e 'require("http").createServer((_, res) => res.end("foo\n")).listen(80)'

ip_bar=$(getent hosts bar.example.org | sed -E -e 's/[[:blank:]]+.*$//')
docker run --name bar --detach --rm -p "$ip_bar:80:80"  node -e 'require("http").createServer((_, res) => res.end("bar\n")).listen(80)'
```
