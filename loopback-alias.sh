#!/usr/bin/env bash

set -euo pipefail

function exit_print_usage() {
    cat <<__HELP__ | sed -E -e 's/^ {8}//'
        Usage: $(basename $0) [options...] <ip>...
          -h, --help         print this help menu and exit
          -d, --delete       remove the IPs aliases from the loopback interface
          -l, --list <file>  print the IPs in the given file and exit
          -f, --file <file>  add the IPs in the given file to the loopback interface

        Where <ip> can be
          an offset (ex: 2) to add to 127.0.0.0
          a offset range (ex: 2-4) to add to 127.0.0.0
          an IP address (ex: 127.0.0.2)
__HELP__
    exit 1
}

function file_parse_ips() {
    if [[ "$#" -ne 1 ]]; then
        echo "Usage: ${FUNCNAME[0]} file"
        exit 1
    fi
    local file="$1"

    sed -E \
        -e 's/#.*$//' \
        -e 's/[[:blank:]].*$//' \
        -e '/::/d' \
        -e '/^[[:blank:]]*$/d' \
        -e '/^127[.]/!d' \
        -e '/^127[.]0[.]0[.]1$)?/d' \
        "$file" | sort | uniq
}

# Argument parsing
ifconfig_command="ifconfig_alias"
ips=()
while [[ $# -gt 0 ]]; do
    arg="$1"
    shift

    case "$arg" in
        -h | --help)
            exit_print_usage
        ;;

        -f | --file)
            file="$1"
            shift
            ips+=(
                $(file_parse_ips "$file")
            )
        ;;

        -d | --delete)
            ifconfig_command="ifconfig_delete"
        ;;

        -l | --list)
            file="$1"
            shift
            file_parse_ips "$file"
            exit 0
        ;;

        *)
            # Parse the various forms of arguments that derrive to an IP address
            if [[ $arg =~ ^[1-9][0-9]*$ ]]; then
                # Number (e.g. 2) to be used an offset for 127.0.0.0
                ips+=("127.0.0.$arg")
            elif [[ $arg =~ ^[1-9][0-9]*-[1-9][0-9]*$ ]]; then
                # Range (e.g. 2-5) to be used as offsets for 127.0.0.0
                from="${arg%-*}" 
                to="${arg#*-}"
                for ((i=${from}; i<=${to}; i++)); do
                    ips+=("127.0.0.$i")
                done
            elif [[ -e "$arg" ]]; then
                # If there's a file that matches we take the IP addresses from there
                ips+=($(file_parse_ips))
            elif [[ $arg =~ ^127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
                # An IP address
                ips+=("$arg")
            else
                # Can't parse the argument
                echo "Unknown argument '$arg'"
                exit 1
            fi
        ;;
    esac
done

function ifconfig_alias() {
    local ip="$1"
    local command=(ifconfig lo0 alias "$ip" up)
    echo "${command[@]}"
    sudo "${command[@]}"
}

function ifconfig_delete() {
    local ip="$1"
    local command=(ifconfig lo0 inet "$ip" delete)
    echo "${command[@]}"
    sudo "${command[@]}"
}


# Perform the command
for ip in "${ips[@]}"; do
    "$ifconfig_command" "$ip"
done
